#include <string>
#include <concepts>

template <typename LHS, typename RHS>
concept CommonWith = std::common_with<LHS, RHS>;

template <typename T1, typename T2>
    requires CommonWith<T1, T2>
void fun2(T1 &&t1, T2 &&t2)
{}


template <typename LHS, typename RHS>
concept CommonRef = std::common_reference_with<LHS, RHS>;

template <typename T1, typename T2>
    requires CommonRef<T1, T2>
void fun(T1 &&t1, T2 &&t2)
{}

struct B
{};

struct D1: public B
{
};

struct D2: public B
{
};

int main()
{
    fun(4, 'a');
    fun(4.5, 'a');

    D1 d1;
    B b;

    fun(b, d1);   // OK
    fun(d1, b);   // OK
    fun(B{}, d1);   // OK
    fun(d1, B{});   // OK
    fun(b, D1{});   // OK
    fun(D1{}, b);   // OK
    fun(B{}, D1{});   // OK
    fun(D1{}, B{});   // OK

    fun2(B{}, D2{});   // OK
}
