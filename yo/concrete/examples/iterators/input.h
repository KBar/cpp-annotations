    #include <iterator>

    struct InputIterator
    {
        using iterator_category = std::input_iterator_tag;
        using difference_type   = std::ptrdiff_t;
        using value_type        = int;
        using pointer           = value_type *;
        using reference         = value_type &;

        private:
            int d_value;

        public:
            InputIterator(int init);

// standard:
            bool operator==(InputIterator const &other) const;
            bool operator!=(InputIterator const &other) const;
            int const &operator*() const;
            InputIterator &operator++();

// consider:
            int const *operator->() const;
    };

InputIterator::InputIterator(int init)
:
    d_value(init)
{}

bool InputIterator::operator!=(InputIterator const &other) const
{
    return d_value != other.d_value;
}

bool InputIterator::operator==(InputIterator const &other) const
{
    return d_value == other.d_value;
}

InputIterator &InputIterator::operator++()
{
    return *this;
}

int const &InputIterator::operator*() const
{
    return d_value;
}
