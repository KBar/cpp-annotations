#ifndef INCLUDED_START_
#define INCLUDED_START_

#include "../../promisebase/promisebase.h"

#include "../awaiter/awaiter.h"

//start
struct Start
{
    struct State: public PromiseBase<Start, State>
    {
        template <typename Handler>
        Awaiter await_transform(Next<Handler> &&next);
    };

    using Handle = std::coroutine_handle<State>;

    std::coroutine_handle<State> d_handle;

    public:
        using promise_type = State;

        explicit Start(Handle handle);
        ~Start();

        Handle handle() const;
        void go();
};

template <typename Handler>
Awaiter Start::State::await_transform(Next<Handler> &&next)
{
    return Awaiter{ next.d_handle };
}

//inline void Start::State::setHandle(std::coroutine_handle<> handle)
//{
//    d_handle = handle;
//}

inline Start::Handle Start::handle() const
{
    return d_handle;
}

extern Start g_start;

#endif
