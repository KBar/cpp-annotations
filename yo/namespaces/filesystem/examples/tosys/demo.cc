#include <iostream>
#include <filesystem>
#include <chrono>

#include <bobcat/exception>

#include "clocktimeconv"

using namespace std;
using namespace chrono;
using namespace filesystem;
using namespace FBB;

int main(int argc, char **argv)
try
{
    error_code ec;

    // comment out the 2nd clock_time_conversion template and you get the
    // first:

    time_t seconds = system_clock::to_time_t(
        clock_time_conversion<system_clock, system_clock>{}(
                                                    system_clock::now()
                                                           )
                            );


    cout << put_time(localtime(&seconds), "%c") << '\n';

    auto sysTime = __file_clock::to_sys(__file_clock::now());

    time_t sysSecs = system_clock::to_time_t(sysTime);

    cout << put_time(localtime(&sysSecs), "%c") << '\n';


//    seconds = system_clock::to_time_t(
//        clock_time_conversion<system_clock, __file_clock>{}(
//                                                    __file_clock::now()
//                                                           )
//                            );
//
//    cout << put_time(localtime(&seconds), "%c") << '\n';

}
catch (exception const &exc)
{
    cout << exc.what() << '\n';

}
